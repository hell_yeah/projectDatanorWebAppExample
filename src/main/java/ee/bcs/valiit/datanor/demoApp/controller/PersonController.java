/*
* PersonController.java is service REST API.
* through API i am able to manage with HTML requests personList data.
* Listens URL requests.
* */

package ee.bcs.valiit.datanor.demoApp.controller;

import ee.bcs.valiit.datanor.demoApp.model.Person;
import ee.bcs.valiit.datanor.demoApp.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
//log.info("Kas kood jõuab siia? PersonalController.java"); // Better way to log compared with sout

@RestController
// gives for PersonController class a benefit
// Declares, that in this class are methods, what respond to REST urls
public class PersonController {

    @Autowired PersonService personService;
    @GetMapping("/person/getbyid/{socialSecurityId}")
    /*
        If request comes to this url, getPerson(Long) method will be overloaded.
        Spring tries to parse method argument to Long. If not successful, spring handles the error.
        Browser knows only get method. Thats why we use in put method the legendary Postman.
     */
    public Person getPersonSecurityId(@PathVariable Long socialSecurityId){
        return personService.getPerson(socialSecurityId);
    }

    @GetMapping("/person/getbyname/{firstName}")
    // If request comes to this url, getPerson(String) method will be overloaded.
    public String getPerson(@PathVariable String firstName){
        return personService.getPerson(firstName);
    }

    @GetMapping("/person/all-sec")
    // If request comes to this url, all socialSecurityId are served. In real life bad idea!
    public String getAllSecurityId(){
        return personService.getAllSecurityId();
    }

    @GetMapping("/person/all-names")
    // If request comes to this url, all names are served. In real life bad idea!
    public String getAllNames(){
        return personService.getAllNames();
    }


    @PostMapping("/person/{socialSecurityId}")
    /*
        Postman is used to test this method. Updates personList member
        * Choose request type
        * In URL describe socialSecurityId
        * In the body raw data type json data
            eg : {"socialSecurityId": "310123456", "firstName": "Miki", "lastName": "Hiinast"}
        * data is updated according to data inside json data
     */
    public boolean updatePerson(@PathVariable Long socialSecurityId, @RequestBody Person person){
        return personService.updatePersonPost(socialSecurityId, person);
    }


    @PutMapping("/person/{socialSecurityId}/{firstName}/{lastName}")
    /*
    * Postman is used to test this method. Adds new member to personList
        * Choose request type
        * In URL describe socialSecurityId
        * In the body raw data type json data
            eg : {"socialSecurityId": "310123456", "firstName": "Miki", "lastName": "Hiinast"}
        * data is updated according to data inside json data
    * */
    public boolean addPerson(@PathVariable Long socialSecurityId, @PathVariable String firstName, @PathVariable String lastName){
        return personService.addPersonPut(socialSecurityId, firstName, lastName);
    }



    @RequestMapping("/person/{firstName}/{lastName}")
    /*
    * Makes request and returns json data. Does not
    * */
    public Person getName(@PathVariable String firstName, @PathVariable String lastName){
        return personService.getPersonFirstLastNames(firstName, lastName);
    }
}
