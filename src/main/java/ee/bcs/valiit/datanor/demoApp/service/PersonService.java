/*
* Person service is necessary to handle and maintain personList.
* (access data in ram (or dB))
* */


package ee.bcs.valiit.datanor.demoApp.service;

import ee.bcs.valiit.datanor.demoApp.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j // parem logger kui sout
@Service
public class PersonService {

    List<Person> personList = new ArrayList<>();

    @PostConstruct
    public void init() {

        Person firstP = new Person();
        firstP.setFirstName("Miki");
        firstP.setLastName("Hiir");
        firstP.setSocialSecurityId(310123456L);

        Person secondP = new Person();
        secondP.setFirstName("Hagar");
        secondP.setLastName("Hirmus");
        secondP.setSocialSecurityId(340123456L);

        Person thirdP = new Person();
        thirdP.setFirstName("Lady");
        thirdP.setLastName("Gaga");
        thirdP.setSocialSecurityId(480123456L);

        personList.add(firstP);
        personList.add(secondP);
        personList.add(thirdP);
    }



    /*
           personList HANDLING METHODS
     */

    // region < GET METHOD'S >
    public Person getPerson(Long socialSecurityId){
        for(Person person : personList){
            //if(person.getSocialSecurityId() == socialSecurityId) // if in Person.java is soc.SecId  primitive type
            if(person.getSocialSecurityId().equals(socialSecurityId)) // if in Person.java is soc.SecId is Long class
            {
                return person;
            }
        }
        return null;
    }

    public String getPerson(String firstName){
        StringBuilder names = new StringBuilder();
        String name = new String();

        for(Person person : personList){
            name = person.getFirstName();
            if(name.equals(firstName)) // if in Person.java is soc.SecId is Long class
            {
                names.append(person.getSocialSecurityId() + " " + name + " " + person.getLastName() + "<br />");
            }
        }

        if(names.length() > 0){
            return names.toString();
        }
        else {
            return null;
        }
    }

    public String getAllSecurityId(){
        StringBuilder allSecurityId = new StringBuilder("All socialSecurityId's: <br />");

        for (Person person : personList){
            allSecurityId.append(person.getSocialSecurityId()).append("<br />");
        }
        return allSecurityId.toString();
    }

    public String getAllNames(){
        StringBuilder allNames = new StringBuilder("All names: <br />");

        for (Person person : personList){
            allNames.append(person.getFirstName())
                .append(" ")
                .append(person.getLastName())
                .append("<br />");
        }
        return allNames.toString();
    }
    // endregion


    // Add new person to runtime object [personList], PUT method
    public boolean addPersonPut(Long socialSecurityId, String firstName, String lastName){
        /*
        * Add new person to personList, if there is not matching socialSecurityId.
        * SocialSecurityId is unique.
        * Returns true/false, if member is added or not.
        * */
        Person p = new Person();
        p.setSocialSecurityId(socialSecurityId);
        p.setFirstName(firstName);
        p.setLastName(lastName);

        boolean personAddedFlag = false;
        Person foundPerson = null;

        for(Person person : personList){
            if(person.getSocialSecurityId().equals(socialSecurityId)){
                foundPerson = person;
                // Match, no need to continue.
                break;
            }
        }

        if(foundPerson == null){
            personList.add(p);
            personAddedFlag = true;
        }

        return personAddedFlag;
    }



    // Update person information, POST method.
    public boolean updatePersonPost(Long socialSecurityId, Person p){
        /*
        * Updates personList member, if request and
        * personList member socialSecurityId's are equal.
        * Returns true/false, if member is updated or not.
        * */

        boolean personUpdatedFlag = false;
        Person foundPerson = null;

        for(Person person : personList){
            if(person.getSocialSecurityId().equals(socialSecurityId)){
                foundPerson = person; // now foundPerson refers to personList 'person' item.
                break;
            }
        }

        if(foundPerson != null){
            foundPerson.setFirstName(p.getFirstName());
            foundPerson.setLastName(p.getLastName());
            personUpdatedFlag = true;
        }

        return personUpdatedFlag;
    }


    /*
    * Testing URL request with name and surname.
    * Auto-generates random social security number.
    * Based on GET method. Returns person object json data,
    * but does not modify/edit personList!!!!
    */
    public Person getPersonFirstLastNames(String firstName, String lastName){

        Person p = new Person();
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setSocialSecurityId(ThreadLocalRandom.current().nextInt(111111111, 999999999));
        return p;
    }
}
