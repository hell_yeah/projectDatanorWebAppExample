package ee.bcs.valiit.datanor.demoApp;

/*
* https://docs.spring.io/spring/docs/5.1.0.RC2/spring-framework-reference/web.html#mvc-ann-requestmapping-uri-templates
* https://www.w3schools.com
*
*
* Homework tasks:
* Get request's:
* 	Added overload method and modified link
* 	http://localhost:8080/person/getbyid/140123456	//returns person data, link argument social security id
*	http://localhost:8080/person/getbyname/Miki		//returns person data, link argument first name
*
* HTTP POST /person/{socialSecurityId}
* 	Send request in Postman to update member data:
* 	** request type POST
* 	** set header: key = "Content-Type", value = "application/json"
* 	** URL: http://localhost:8080/person/310123456
* 	** BODY (json statement): {"socialSecurityId": "310123456", "firstName": "Miki", "lastName": "Hiinast"}
* 	** send & it should return true
*
* HTTP GET /person/{firstName}
*	Send request in Postman to create member:
*	** request type PUT
*	** URL: http://localhost:8080/person/322123456/Jaak/Tamm
*
*
* Additional learning stuff:
* http://localhost:8080/person/all-sec
* http://localhost:8080/person/all-names
* http://localhost:8080/person/Jaak/Tamm // test request for getting first responses. Does not update personList.
* */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAppApplication.class, args);
	}
}
